<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'hotel'); // база, которую мы создали

/** Имя пользователя MySQL */
define('DB_USER', 'root'); // имя пользователя бд

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'V3ufG0k1c'); // пароль

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&dY^jgR:Ge,-8m,Jrjs*l(Hnfh7hWI1N.mr36DMK`VN8FB:%LGf.-+kUhgpr!odv');
define('SECURE_AUTH_KEY',  'cHK$>m4_3cCNz]Q_~*LK^w(3J+s^>rdWKNo<xI|NxM2JZN#* ]5R:.,/0z?CfX*h');
define('LOGGED_IN_KEY',    'q`C$2 2P/i:K13MQj 3!H@D#C_]BU<t}])Fgh@<9 4NWdHoa<D^U}A!D7eVKNLUC');
define('NONCE_KEY',        'kgw+uhxvbO3?Fx7~Y8f|i#]c0rMB4pSvN1kJ&$~D6h*/CVc)gxVRV{qGd,V/qKh`');
define('AUTH_SALT',        'j`fOGSdx^c|vv`&R.;rOapahnXRV6yX;pX!Deo?0Sn4o~3~=jk>&W*0<JaC]sj@O');
define('SECURE_AUTH_SALT', 'l-ve<j<g6J]bSBjZ(TP<Q~x~PIw[4:wus+An<; %eahZvgq$M<|j(cT5rPnC$.L|');
define('LOGGED_IN_SALT',   'w&_0Z~YM5J>0v/y,l0Am@ZSNU+tY>.Q3)_L+=`_nI|7_6kHHu0s:X(u=eLMu~mz{');
define('NONCE_SALT',       '$LdD<w$&LuAmIc`%]=I*s8_Is=AuPOmSBxaUWa~j>^ipRzKUY{|d]L&_r`*m;5_L');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
