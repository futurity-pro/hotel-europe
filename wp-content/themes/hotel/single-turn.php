<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<section class="header-bottom">
    <div class="baner">
      <?php the_post_thumbnail() ?>
    </div>
</section><!--header bottom-->
</header><!--header-->
<section class="wrapper">
  <aside class="left-box">
    <?php get_sidebar() ?>
    <nav class="menu spa">
      <?php $serv = get_field('service_id'); $serviceId = $serv->ID; $turnId = $post->ID; ?>
      <h2><?php echo get_the_title($serv->ID); ?></h2>
      <ul>
        <?php query_posts('post_type=turn&orderby=date&order=ASC'); while(have_posts()): the_post(); $s = get_field('service_id'); if($serv->ID==$s->ID): ?>
        <?php if($turnId == $post->ID): ?>
        <li class="active"><?php the_title(); ?></li>
        <?php else: ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endif; ?>
        <?php endif; endwhile; wp_reset_query(); ?>
      </ul>
      <a href="<?php get_permalink($serv->ID) ?>" class="btn-all">О <?php echo get_the_title($serv->ID); ?></a>
    </nav><!--menu-->
  </aside><!--left-box-->
  <article class="content-massage">
    <article class="post-info post-title turn">
      <h2><?php the_title() ?></h2>
      <?php the_content() ?>
    </article>
  </article><!--content-->
</section><!--wrapper-->
</section><!--page-->
<?php endwhile; ?>
<?php get_footer(); ?>