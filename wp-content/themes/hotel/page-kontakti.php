<?php get_header(); ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
</script><script type="text/javascript">
    function initialize() {
	var mapOptions = {
	center:new google.maps.LatLng(44.5866113, 34.3513614),
	zoom:16,
	mapTypeId:google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	var image = '<?php echo get_template_directory_uri(); ?>/img/pin.png';
	var myLatLng = new google.maps.LatLng(44.5866113, 34.3513614);
	var beachMarker = new google.maps.Marker({
	position:myLatLng,
	map:map,
	icon:image
	});
    }
    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<section class="header-bottom">
    <div class="map">
	<div id="map_canvas" style="width:100%; height:100%;position:absolute;"></div>
	<article class="contact-map">
	<h2>Контакты</h2>
	<p>98542, АР КРЫМ, г. Алушта,</p>
	<p>пгт. Партенит, ул. Г. Васильченко, 6</p>
	</article>
	<!--contact map-->
    </div>
    <!--map--></section><!--header bottom--></header><!--header-->
	<section class="wrapper">
    <div class="content-contact">
	<article class="post-contact-hotel">
	<h3>Отель</h3>
	<p>+38(0652) 79–02–29</p>
	<p>+38(095) 124–60–77</p>
	</article>
	<!--post-contact-hotel-->
	<article class="post-contact">
	<figure>
	<figcaption>
	<h3>Ресторан</h3>
	<p>+38(0652) 79–02–29</p>
	<p>+38(095) 124–60–77</p>
	</figcaption>
	<img src="<?php echo get_template_directory_uri(); ?>/img/contact-img1.jpg" width="190" height="240" alt="Контакты">
	</figure>
	</article>
	<article class="post-contact">
	<figure>
	<figcaption>
	<h3>Фитнесс-центр</h3>
	<p>+38(095) 124–60–77</p>
	</figcaption>
	<img src="<?php echo get_template_directory_uri(); ?>/img/contact-img2.jpg" width="190" height="240" alt="Контакты">
	</figure>
	</article>
	<article class="post-contact">
	<figure>
	<figcaption class="spa-bg">
	<h3>Салон красоты</h3>
	<p>+38(095) 124–60–77</p>
	</figcaption>
	<img src="<?php echo get_template_directory_uri(); ?>/img/contact-img3.jpg" width="190" height="240" alt="Контакты">
	</figure>
	</article>
	<div class="contact-form">
	<div class="form-title">
	<h2>Напишите нам сообщение</h2>
	</div>
	<!--form title-->
	<form action="">
	<div class="form-top">
	<div class="input-holder">
	<label for="name">
	<p>Имя</p></label>
	<input type="text" id="contact_name">
	</div>
	<div class="input-holder">
	<label for="email">
	<p>Email</p></label>
	<input type="text" id="contact_email">
	</div>
	</div>
	<div class="form-bottom">
	<div class="input-holder">
	<label for="">
	<p>Сообщение</p>
	</label>
	<textarea name="message" id="message" cols="30" rows="10"></textarea>
	<a id="contact-submit" href="#dialog" name="modal" class="btn-contact">Отправить</a>
	</div>
	</div>
	</form>
	<script>
	$('#contact-submit').click(function () {
	$.post('<?php echo get_template_directory_uri(); ?>/mail.php',
	{name:$('#contact_name').val(), email:$('#contact_email').val(), message:$('#message').val()});
	});
	</script>
	</div>
    </div><!--content contact-->
	</section><!--wrapper-->
	</section><!--page-->
	<?php get_footer(); ?>