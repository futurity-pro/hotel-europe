<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<?php $serviceId = $post->ID; ?>
<section class="header-bottom">
    <div class="baner">
      <?php the_post_thumbnail() ?>
    </div>
  </section><!--header bottom-->
</header><!--header-->
<section class="wrapper">
  <aside class="left-box">
    <?php get_sidebar() ?>
  </aside><!--left box-->
  <article class="content-spa">
    <article class="content-holder full">
      <article class="post-box">
        <h2><?php the_title() ?></h2>
        <?php the_content() ?>
      </article>
    </article>
    <div class="gallery">
      <?php query_posts("post_type=turn"); $i=0; while(have_posts()): the_post(); $serv=get_field('service_id'); if($serv->ID==$serviceId): $i++; if($i>4) $i=1;?>
      <figure class="gallery-box <?php if($i==1):?>margin<?php elseif($i==4): ?>margin text-left<?php endif; ?>">
        <?php $photo = get_field('photo'); ?>
        <?php ob_start(); ?>
        <figcaption>

        </figcaption>
        <?php $caption_html = ob_get_clean(); ?>
        <?php if ($i<4) echo $caption_html; ?>
        <div class="btn-img">
          <?php if ($i<4): ?>
          <p><a href="<?php the_permalink() ?>"><?php the_title() ?></a></p>
          <?php endif; ?>
          <a href="<?php echo $photo['sizes']['large']; ?>"
            title="<?php the_title() ?>">
          <img src="<?php echo $photo['sizes']['thumbnail']; ?>" height="120" width="190" alt="<?php the_title() ?>">
          </a>
          <?php if ($i==4): ?>
          <p><a href="<?php the_permalink() ?>"><?php the_title() ?></a></p>
          <?php endif; ?>
        </div><!--img-holder-->
        <?php if ($i==4) echo $caption_html; ?>
      </figure>
      <?php endif; endwhile; wp_reset_query(); ?>
    </div><!--gallery-->
    
  </article><!--content-->
</section><!--wrapper-->
</section><!--page-->
<?php endwhile; ?>
<?php get_footer(); ?>