<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<?php $barId = $post->ID; ?>
<section class="header-bottom">
    <div class="baner">
      <?php the_post_thumbnail() ?>
    </div>
  </section><!--header bottom-->
</header><!--header-->
<section class="wrapper">
  <aside class="left-box">
    <nav class="menu offer">
      <h2>Бары и рестораны</h2>
      <ul>
        <?php query_posts('post_type=bar&orderby=date&order=ASC'); while(have_posts()): the_post(); ?>
        <?php if($barId == $post->ID): ?>
        <li class="active"><?php the_title(); ?></li>
        <?php else: ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endif; ?>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
      <a href="/bari-i-restorani/" class="btn-all">Все бары и рестораны</a>
    </nav><!--menu-->
  </aside><!--left box-->
  <article class="content-rooms">
    <article class="content-holder full">
      <article class="post-box">
        <h2><?php the_title() ?></h2>
        <?php the_content(); ?>
      </article>
    </article>
    <?php if(get_field('kidmenu_url')): ?>
    <a href="<?php the_field('kidmenu_url') ?>" class="btn-kidmenu">Детское меню</a>
    <?php endif; ?>
    <div class="gallery">
      <?php query_posts("post_type=photo"); $i=0; while(have_posts()): the_post(); $bar=get_field('bar_id'); if($bar->ID==$barId): $i++; if($i>4) $i=1;?>
      <figure class="gallery-box <?php if($i==1):?>margin gallery-box-center<?php elseif($i==4): ?>margin text-left<?php endif; ?>">
        <?php $photo = get_field('photo'); ?>
        <?php ob_start(); ?>
        <figcaption>
          <p><?php the_title() ?></p>
        </figcaption>
        <?php $caption_html = ob_get_clean(); ?>
        <?php if ($i<4) echo $caption_html; ?>
        <div class="img-holder">
          <a href="<?php echo $photo['sizes']['large']; ?>"
            title="<?php the_title() ?>" class="litebox">Увеличить фото</a>
          <img src="<?php echo $photo['sizes']['thumbnail']; ?>" width="190" alt="<?php the_title() ?>">
        </div><!--img-holder-->
        <?php if ($i==4) echo $caption_html; ?>
      </figure>
      <?php endif; endwhile; wp_reset_query(); ?>
    </div><!--gallery-->
    <div class="features">
      <div class="features-holder">
        <div class="line-holder">
          <?php if(get_field('phone')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img1.png" width="60" height="55" alt="Телефон">
            <figcaption>
              <h4>Телефон</h4>
              <p><?php the_field('phone') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('work_time')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img2.png" width="60" height="55" alt="Время работы">
            <figcaption>
              <h4>Время работы</h4>
              <p><?php the_field('work_time') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('kitchen')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img3.png" width="60" height="55" alt="Кухня">
            <figcaption>
              <h4>Кухня</h4>
              <p><?php the_field('kitchen') ?>
              </p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('rooms')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img4.png" width="60" height="55" alt="Залы">
            <figcaption>
              <h4>Залы</h4>
              <p><?php the_field('rooms') ?>
              </p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('music')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img5.png" width="60" height="55" alt="Музыка">
            <figcaption>
              <h4>Музыка</h4>
              <p><?php the_field('music') ?></p>
            </figcaption>
          </figure>
        <?php endif; ?>
          <?php if(get_field('dance')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img6.png" width="60" height="55" alt="Танцпол">
            <figcaption>
              <h4>Танцпол</h4>
              <p><?php the_field('dance') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('show')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img7.png" width="60" height="55" alt="Шоупрограмма">
            <figcaption>
              <h4>Шоупрограмма</h4>
              <p><?php the_field('show') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('smoke')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img8.png" width="60" height="55" alt="Кальян">
            <figcaption>
              <h4>Кальян</h4>
              <p><?php the_field('smoke') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('wifi')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img9.png" width="60" height="55" alt="WiFi">
            <figcaption>
              <h4>WiFi</h4>
              <p><?php the_field('wifi') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('summ')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img10.png" width="60" height="55" alt="Сумма счета">
            <figcaption>
              <h4>Сумма счета</h4>
              <p><?php the_field('summ') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('discount')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img11.png" width="60" height="55" alt="Скидки">
            <figcaption>
              <h4>Скидки</h4>
              <p><?php the_field('discount') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('credits')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img12.png" width="60" height="55" alt="Кредитные карты">
            <figcaption>
              <h4>Кредитные карты</h4>
              <p><?php the_field('credits') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('no_smoke')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img13.png" width="60" height="55" alt="Зона для не курящих">
            <figcaption>
              <h4>Зона для не курящих</h4>
              <p><?php the_field('no_smoke') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('kids')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img14.png" width="60" height="55" alt="Меню для детей">
            <figcaption>
              <h4>Меню для детей</h4>
              <p><?php the_field('kids') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
          <?php if(get_field('dog')!='-'): ?>
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/img/bar-img15.png" width="60" height="55" alt="Еда на вынос">
            <figcaption>
              <h4>Еда на вынос</h4>
              <p><?php the_field('dog') ?></p>
            </figcaption>
          </figure>
          <?php endif; ?>
        </div><!--line holder 2-->
      </div>
    </div>
  </article><!--content-->
</section><!--wrapper-->
</section><!--page-->
<?php endwhile; ?>
<?php get_footer(); ?>