<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<section class="header-bottom">
    <div class="baner">
      <?php the_post_thumbnail() ?>
    </div>
  </section><!--header bottom-->
</header><!--header-->
<section class="wrapper">
  <aside class="left-box">
    <?php get_sidebar() ?>
    <nav class="room-sidebar">
    <?php
        $rooms = get_posts(array(
              'post_type' => 'room',
              'numberposts' => -1,
              'orderby'        => 'date',
              'order'           => 'ASC',
              ));
     ?>
      <h2><?php $cur = get_field('category'); echo get_the_title($cur->ID) ?></h2>
      <ul>
        <?php
              foreach( $rooms as $room) {
              $cat = get_field('category', $room->ID);
              if($cat->ID == $cur->ID ) { if($room->ID==$post->ID){ ?>
              <li><?php echo get_the_title($room->ID) ?></li>    
              <?php } else { ?>
              <li><a href="<?php echo get_permalink($room->ID) ?>"><?php echo get_the_title($room->ID) ?></a></li>    
              <?php }}} 
        ?>
      </ul>
    </nav>
  </aside><!--left box-->
  <article class="content-rooms">
    <article class="content-holder full">
      <article class="post-box">
        <h2><?php the_title() ?></h2>
        <?php the_content() ?>
      </article>
    </article>
    <article class="room-in">
      <div class="room-in-content">
          <h2>В стоимость номера включено</h2>
          <?php the_field('include') ?>
      </div>
    </article>
    <div class="gallery-room">
      <?php $barId = $post->ID; ?>
      <?php query_posts("post_type=photo&posts_per_page=-1&nopaging=true"); $i=0; while(have_posts()): the_post(); $bar=get_field('bar_id'); if($bar->ID==$barId): $i++; if($i>4) $i=1;?>
      <figure class="gallery-box <?php if ($i>2) echo 'text-left'; else echo 'text-right'; ?>">
        <?php $photo = get_field('photo'); ?>
        <?php ob_start(); ?>
        <figcaption>
          <p><?php the_title() ?></p>
        </figcaption>
        <?php $caption_html = ob_get_clean(); ?>
        <?php if ($i>2) echo $caption_html; ?>
        <div class="img-holder">
            <a href="<?php echo $photo['sizes']['large']; ?>"
            title="<?php the_title() ?>" class="litebox">Увеличить фото</a>
            <img src="<?php echo $photo['sizes']['thumbnail']; ?>" width="190" alt="<?php the_title() ?>">
        </div>
        <?php if ($i<=2) echo $caption_html; ?>
      </figure>
      <?php endif; endwhile; wp_reset_query(); ?>
    </div><!--gallery-->
    <article class="room-description">
      <div class="list">
      <h2>Комплектация номера</h2>
      <?php the_field('complect') ?>
      </div>
      <div class="list">
      <h2>В ванной комнате</h2>
      <?php the_field('bath') ?>
      </div>
    </article>
  </article><!--content-->
</section><!--wrapper-->
</section><!--page-->
<?php endwhile; ?>
<?php get_footer(); ?>
