<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
<section class="header-bottom">
    <div class="slideshow-holder">
        <nav class="slideshow-nav">
            <div id="nav"></div>
        </nav>
        <div class="slideshow">
            <?php
                if ( $images = get_posts(array(
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'orderby'        => 'title',
                    'order'           => 'ASC',
                    'post_mime_type' => 'image',
                    'exclude' => $thumb_ID,
                    )))
                {
                    foreach( $images as $image ) {
                        $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                        echo '<img src="'.$attachmentImage[0].'">';
                    }
                }
            ?>
        </div>
    </div>
    <!--slideshow holder-->
</section><!--header bottom-->
</header><!--header-->
<section class="wrapper">
    <aside class="left-box">
        <?php get_sidebar() ?>
        <nav class="menu">
            <h2>Спецпредложения</h2>
            <ul>
              <?php query_posts('post_type=offer&orderby=date&order=ASC'); while(have_posts()): the_post(); ?>
              <?php if($barId == $post->ID): ?>
              <li class="active"><?php the_title(); ?></li>
              <?php else: ?>
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php endif; ?>
              <?php endwhile; wp_reset_query(); ?>
            </ul>
            <a href="/spezpredlozheniya/" class="btn-all">Все спецпредложения</a>
        </nav>
        <!--menu-->
        <article class="news">
            <?php dynamic_sidebar('home-page-sidebar'); ?>
        </article>
        <!--news-->
    </aside>
    <!--left-box-->

    <article class="content hotel">
        <div class="post-info">
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </div>
    </article>
    <!--content-->

</section><!--wrapper-->
</section><!--page-->
<?php endwhile; ?>
<?php get_footer(); ?>