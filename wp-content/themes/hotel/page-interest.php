<?php get_header(); ?>
<section class="header-bottom">
					<div class="slideshow-holder">
						<nav class="slideshow-nav">
							<div id="nav"></div>
						</nav>
						<div class="slideshow">
							 <?php
                if ( $images = get_posts(array(
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'orderby'        => 'title',
                    'order'           => 'ASC',
                    'post_mime_type' => 'image',
                    'exclude' => $thumb_ID,
                    )))
                {
                    foreach( $images as $image ) {
                        $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                        echo '<img src="'.$attachmentImage[0].'">';
                    }
                }
				query_posts("cat=6&posts_per_page=3");
				while (have_posts()) : the_post();
					if (get_field('main_news') == 'yes') $main_afisha_post_id = $post->ID;
				endwhile;
				wp_reset_query();
              ?>
						</div>
					</div><!--slideshow holder-->
				</section><!--header bottom-->
			</header><!--header-->
			<section class="wrapper">
				<article class="content-rooms">
			        <article class="shortnews block">

                        <article class="last">
                            <h2>Новости</h2>
                            <?php /*Вывод рубрики новости*/ query_posts("cat=5&posts_per_page=3"); while (have_posts()) : the_post(); ?>
                            <?php if (get_field('main_news') == 'yes') $main_news_post_id = $post->ID?>
                                <div class="row">
                                    <span class="data"><?php the_time('d.m.Y'); ?></span>
                                    <h4>
                                        <a href="<?php the_permalink(); ?>"><?php trim_title_words(8, ' ...'); ?></a>
                                    </h4>
                                    </div>
                                </div><!-- row -->
                            <?php endwhile; wp_reset_query();?>

                        </article><!-- news-last -->
                        <article class="interestingly">
                            <div class="interestingly-holder">
                                <a class="data" href="<?php echo get_permalink($main_news_post_id)?>"><?php echo get_the_time('d.m.Y',$main_news_post_id)?></a>
                                <h3><a href="<?php echo get_permalink($main_news_post_id)?>"><?php echo wp_trim_words(get_the_title($main_news_post_id),8, ' ...')?></a></h3>
                                <a href="<?php echo get_permalink($main_news_post_id)?>" class="briefly">
                                    <p><?php echo wp_trim_words(get_post_field('post_content', $main_news_post_id),24,' ...')?></p>
                                </a>
                            </div>
                            <figure class="img img_1">
                                <a href="<?php site_url(); ?>/category/interest/news" class="button" id="img_1">
                                    <span>Все новости</span>
                                </a>
                                <a href="<?php echo get_permalink($main_news_post_id)?>"><?php echo get_the_post_thumbnail($main_news_post_id, 'full'); ?></a>
                            </figure><!-- news-img -->

                            <?php /*End of Вывод рубрики новости*/ ?>
                        </article><!-- news-interestingly -->
			        </article><!-- news-block -->
			        <?php /*Вывод рубрики афиша*/?>
                    <article class="doings block">
                        <article class="interestingly">
                            <div class="interestingly-holder">
                                <a class="data" href="<?php echo get_permalink($main_afisha_post_id)?>"><?php echo get_the_time('d.m.Y',$main_afisha_post_id)?></a>
                                <h3><a href="<?php echo get_permalink($main_afisha_post_id)?>"><?php echo wp_trim_words(get_the_title($main_afisha_post_id),8, ' ...')?></a></h3>
                                <a href="<?php echo get_permalink($main_afisha_post_id)?>" class="briefly">
                                    <p><?php echo wp_trim_words(get_post_field('post_content', $main_afisha_post_id),24,' ...')?></p>
                                </a>
                            </div>
                            <figure class="img img_2">
                                <a href="<?php site_url(); ?>/category/interest/afisha" class="button"  id="img_2">
                                    <span>Все события</span>
                                </a>
                                <a href="<?php echo get_permalink($main_afisha_post_id)?>"><?php echo get_the_post_thumbnail($main_afisha_post_id, 'full'); ?></a>
                            </figure><!-- news-img -->
                        </article><!-- news-interestingly -->
                        <article class="last">
                            <h2>Афиша</h2>
                            <?php query_posts("cat=6&posts_per_page=3"); while (have_posts()) : the_post(); ?>
                                <div class="row">
                                    <span class="data"><?php the_time('d.m.Y'); ?></span>
                                    <h4>
                                        <a href="<?php the_permalink(); ?>"><?php trim_title_words(8, ' ...'); ?></a>
                                    </h4>
                                    </div>
                                </div><!-- row -->
                            <?php endwhile; wp_reset_query();?>

                            <?php /*End of Вывод рубрики афиша*/ ?>
                        </article><!-- news-last -->
                    </article><!-- doings-block -->
                    <article class="crimea-and-secrets block">
                        <article class="last">
                            <h2>Крым и его секреты</h2>
                            <?php query_posts("cat=7&posts_per_page=3"); while (have_posts()) : the_post(); ?>
                            <?php if (get_field('main_news') == 'yes') $main_crimea_post_id = $post->ID?>
                                <div class="row">
                                    <span class="data"><?php the_time('d.m.Y'); ?></span>
                                    <h4>
                                        <a href="<?php the_permalink(); ?>"><?php trim_title_words(8, ' ...'); ?></a>
                                    </h4>
                                    </div>
                                </div><!-- row -->
                            <?php endwhile; wp_reset_query();?>

                        </article><!-- news-last -->
                        <article class="interestingly">
                            <div class="interestingly-holder">
                                <a class="data" href="<?php echo get_permalink($main_crimea_post_id)?>"><?php echo get_the_time('d.m.Y',$main_crimea_post_id)?></a>
                                <h3><a href="<?php echo get_permalink($main_crimea_post_id)?>"><?php echo wp_trim_words(get_the_title($main_crimea_post_id),8, ' ...')?></a></h3>
                                <a href="<?php echo get_permalink($main_crimea_post_id)?>" class="briefly">
                                    <p><?php echo wp_trim_words(get_post_field('post_content', $main_crimea_post_id),24,' ...')?></p>
                                </a>
                            </div>
                            <figure class="img img_3">
                                <a href="<?php site_url(); ?>/category/interest/krym-i-ego-sekrety" class="button" id="img_3">
                                    <span>Все секреты</span>
                                </a>
                                <a href="<?php echo get_permalink($main_crimea_post_id)?>"><?php echo get_the_post_thumbnail($main_crimea_post_id, 'full'); ?></a>
                            </figure><!-- news-img -->
                            <?php /*End of Вывод рубрики афиша*/ ?>
                        </article><!-- crimea-and-secrets-interestingly -->
                    </article><!-- crimea-and-secrets-block -->
                </article>
			</section><!--wrapper-->
		</section><!--page-->
<?php get_footer(); ?>