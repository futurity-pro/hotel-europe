<div class="calculator block">
	<div class="date-holder">
		<?php get_template_part( 'calculator', 'datepicker' ) ?>
		<?php get_template_part( 'calculator', 'room' ) ?>
	</div><!--date-holder-->
	<?php get_template_part( 'calculator', 'people' ) ?>
	<?php get_template_part( 'calculator', 'order' ) ?>
</div><!--.form-->