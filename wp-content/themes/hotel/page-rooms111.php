<?php get_header(); ?>
<section class="header-bottom">
					<div class="slideshow-holder">
						<nav class="slideshow-nav">
							<div id="nav"></div>
						</nav>
						<div class="slideshow">
							<?php
                if ( $images = get_posts(array(
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'orderby'        => 'title',
                    'order'           => 'ASC',
                    'post_mime_type' => 'image',
                    'exclude' => $thumb_ID,
                    )))
                {
                    foreach( $images as $image ) {
                        $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                        echo '<img src="'.$attachmentImage[0].'">';
                    }
                }
              ?>
						</div>
					</div><!--slideshow holder-->
				</section><!--header bottom-->
			</header><!--header-->
			<section class="wrapper">
        <aside class="left-box">
				<?php get_sidebar() ?>
        </aside><!--left-box-->	
				<?php
            $rooms = get_posts(array(
                  'post_type' => 'room',
                  'numberposts' => -1,
                  'orderby'        => 'date',
                  'order'           => 'ASC',
                  ));
         ?>
				<?php query_posts("page_id=33"); while(have_posts()): the_post(); ?>
				<article class="content-rooms">
				  <article class="rooms standard">
						<article class="rooms-post">
							<h2><?php the_title(); ?></h2>
							<h4><?php the_field('price'); ?></h4>
							<?php the_content(); ?>
							<ul>
							<?php
                    foreach( $rooms as $room) {
                    $cat = get_field('category', $room->ID);
                    if($cat->ID ==$post->ID ) {?>
                    <li><a href="<?php echo get_permalink($room->ID) ?>"><?php echo get_the_title($room->ID) ?></a></li>    
                    <?php }} 
              ?>
              </ul>
						</article>
						<img src="<?php echo get_template_directory_uri(); ?>/img/rooms-img1.jpg" width="190" height="240" alt="Стандарт">
					</article><!--standart-->
					<?php endwhile; ?>
					<?php query_posts("page_id=38"); while(have_posts()): the_post(); ?>
					<article class="rooms halfluxe">
						<article class="rooms-post">
							<h2><?php the_title(); ?></h2>
							<h4><?php the_field('price'); ?></h4>
							<?php the_content(); ?>
							<ul>
							<?php
                    foreach( $rooms as $room) {
                    $cat = get_field('category', $room->ID);
                    if($cat->ID ==$post->ID ) {?>
                    <li><a href="<?php echo get_permalink($room->ID) ?>"><?php echo get_the_title($room->ID) ?></a></li>    
                    <?php }} 
              ?>
						</article>
						<img src="<?php echo get_template_directory_uri(); ?>/img/rooms-img2.jpg" width="380" height="240" alt="Полулюкс">
					</article><!--halfluxe-->
          <?php endwhile; ?>
          <?php query_posts("page_id=41"); while(have_posts()): the_post(); ?>
					<article class="rooms apartments">
						<img src="<?php echo get_template_directory_uri(); ?>/img/rooms-img3.jpg" width="380" height="240" alt="Апартаменты">
						<article class="rooms-post">
							<h2><?php the_title(); ?></h2>
							<h4><?php the_field('price'); ?></h4>
							<?php the_content(); ?>
							<ul>
							<?php
                    foreach( $rooms as $room) {
                    $cat = get_field('category', $room->ID);
                    if($cat->ID ==$post->ID ) {?>
                    <li><a href="<?php echo get_permalink($room->ID) ?>"><?php echo get_the_title($room->ID) ?></a></li>    
                    <?php }} 
              ?>
						</article>						
					</article><!--apartaments-->
					<?php endwhile; ?>
					<?php query_posts("page_id=44"); while(have_posts()): the_post(); ?>
					<article class="rooms luxe">
						<article class="rooms-post">
							<h2><?php the_title(); ?></h2>
							<h4><?php the_field('price'); ?></h4>
							<?php the_content(); ?>
							<ul>
							<?php
                    foreach( $rooms as $room) {
                    $cat = get_field('category', $room->ID);
                    if($cat->ID ==$post->ID ) {?>
                    <li><a href="<?php echo get_permalink($room->ID) ?>"><?php echo get_the_title($room->ID) ?></a></li>    
                    <?php }} 
              ?>
						</article>
						<img src="<?php echo get_template_directory_uri(); ?>/img/rooms-img4.jpg" width="570" height="240" alt="Люкс">
					</article><!--luxe-->
					<?php endwhile; ?>
				</article><!--content-->
			</section><!--wrapper-->
		</section><!--page-->
<?php get_footer(); ?>